import logging
import signal
import time
import requests
import sys
from environs import Env
from typing import Dict, List
import prometheus_client
from prometheus_client import Counter, Gauge, Histogram, start_http_server


class ExporterConfig:
    def __init__(self):
        env = Env()
        self.oncall_exporter_api_url = env("ONCALL_EXPORTER_ONCALL_API_URL", "http://oncall.com")
        self.ip = env("ONCALL_EXPORTER_IP", "0.0.0.0")
        self.port = env.int("ONCALL_EXPORTER_PORT", 9200)
        self.scrape_interval = env.int("ONCALL_EXPORTER_SCRAPE_INTERVAL", 10)
        self.timeout = env.int("ONCALL_EXPORTER_API_TIMEOUT", 5)
        self.log_level = env.log_level("ONCALL_EXPORTER_LOG_LEVEL", logging.DEBUG)
        self.users_count_to_be_stuffed = env.int("ONCALL_EXPORTER_USERS_COUNT_TO_BE_STUFFED", 2)


class Exporter:
    def __init__(self, config: ExporterConfig):
        self.config = config
        self.scrapers = [self.check_health, self.count_users_without_contacts, self.proceed_teams]
        self.ONCALL_HEALTH_STATUS = Gauge("oncall_health_status", "indicates if oncall is reachable at it's mainpage")
        self.ONCALL_API_REQUESTS_TOTAL = Counter("oncall_api_requests_total", "Total count of requests to oncall API")
        self.ONCALL_API_REQUESTS_FAILED_TOTAL = Counter("oncall_api_requests_failed_total",
                                                        "Total count of failed requests to oncall API")

        self.ONCALL_API_REQUESTS_DURATION = Histogram("oncall_api_requests_duration", "oncall api requests duration")
        self.ONCALL_USERS_TOTAL = Gauge("oncall_users_total", "total number of users registered in the system")
        self.ONCALL_TEAMS_TOTAL = Gauge("oncall_teams_total", "total number of active teams present in the system")
        self.ONCALL_USERS_WITHOUT_CONTACTS_GAUGE = Gauge("oncall_users_without_contacts_gauge",
                                                         "total number of users without contact data")

        self.ONCALL_USERS_WITHOUT_PHONE_NUMBER = Gauge("oncall_users_without_phone_number",
                                                       "Number of users without phone data")

        self.ONCALL_TEAMS_UNDERSTAFFED = Gauge("oncall_teams_understaffed",
                                               "total number of teams that do not have at least two members for current"
                                               " or next rotation")

        self.ONCALL_TEAM_ROTATION_STAFF_COUNT = Gauge("oncall_team_rotation_staff_count",
                                                      "members count in given team by rotation"
                                                      "(if rotation does not exist returns 0)",
                                                      ["team_name", "rotation"])

        self.ONCALL_TEAMS_UNREACHABLE_BY_PHONE_TOTAL = Gauge("oncall_teams_unreachable_by_phone_total",
                                                             "total number of teams in which all members"
                                                             "can't be reached by phone on current or next rotation")

        self.ONCALL_TEAM_ROTATION_MEMBERS_UNREACHABLE_BY_PHONE_COUNT = Gauge(
            "oncall_team_rotation_members_unreachable_by_phone_count",
            "number of members in given team on select rotation that do not have phone contact data",
            ["team_name", "rotation"])

    def request_with_metric_inc(self, path: str) -> requests.Response:
        self.ONCALL_API_REQUESTS_TOTAL.inc()
        logging.debug(f"Requesting {path}")
        try:
            with self.ONCALL_API_REQUESTS_DURATION.time():
                resp = requests.get(f"{self.config.oncall_exporter_api_url}{path}", timeout=self.config.timeout)
        except requests.exceptions.ConnectionError:
            logging.error(f"Connection error to {self.config.oncall_exporter_api_url}{path}")
            self.ONCALL_API_REQUESTS_FAILED_TOTAL.inc()
            resp = requests.Response()
            resp.status_code = 503
            return resp

        if resp.status_code != 200:
            logging.error(f"Request to {path} returned {resp.status_code} {resp.text}")
            self.ONCALL_API_REQUESTS_FAILED_TOTAL.inc()
        return resp

    def check_health(self):
        resp = self.request_with_metric_inc("/")
        if resp.status_code == 200:
            self.ONCALL_HEALTH_STATUS.set(1)
        else:
            self.ONCALL_HEALTH_STATUS.set(0)

    def count_users_without_contacts(self):
        resp = self.request_with_metric_inc("/api/v0/users")
        if resp.status_code != 200:
            return
        users = resp.json()
        logging.debug(f"Found {len(users)} users")
        self.ONCALL_USERS_TOTAL.set(len(users))
        failed_users = 0
        no_phone = 0
        for user in users:
            if not user['contacts']:
                logging.debug(f"Found user without contacts: {user['name']}")
                failed_users += 1

            if "call" not in user['contacts'] or user['contacts']['call'] == "":
                logging.debug(f"User {user['name']} does not have phone number")
                no_phone += 1

        self.ONCALL_USERS_WITHOUT_CONTACTS_GAUGE.set(failed_users)
        self.ONCALL_USERS_WITHOUT_PHONE_NUMBER.set(no_phone)

    def get_rotation_members_count(self, event: Dict[str, List]) -> (int, int):
        members_count = 0
        unreachable_count = 0
        for roles in event.values():
            for user in roles:
                members_count += 1
                if "call" not in user['user_contacts'] or user['user_contacts']['call'] == "":
                    unreachable_count += 1
        return members_count, unreachable_count

    def proceed_teams(self):
        resp = self.request_with_metric_inc("/api/v0/teams")
        if resp.status_code != 200:
            return
        teams = resp.json()
        logging.debug(f"Found {len(teams)} teams")
        self.ONCALL_TEAMS_TOTAL.set(len(teams))

        understaffed_teams_count = 0
        unreachable_teams_count = 0
        for team_name in teams:
            team_data = self.request_with_metric_inc(f"/api/v0/teams/{team_name}/summary")
            if team_data.status_code != 200:
                continue
            team_data = team_data.json()
            for event_time in ["current", "next"]:
                event = team_data[event_time]
                if not event:
                    self.ONCALL_TEAM_ROTATION_STAFF_COUNT.labels(team_name, event_time).set(0)
                    self.ONCALL_TEAM_ROTATION_MEMBERS_UNREACHABLE_BY_PHONE_COUNT.labels(team_name, event_time).set(0)
                    continue

                rotation_members_count, unreachable_members_count = self.get_rotation_members_count(event)
                self.ONCALL_TEAM_ROTATION_STAFF_COUNT.labels(team_name, event_time).set(rotation_members_count)
                self.ONCALL_TEAM_ROTATION_MEMBERS_UNREACHABLE_BY_PHONE_COUNT.labels(team_name, event_time).set(
                    unreachable_members_count)

                if rotation_members_count < self.config.users_count_to_be_stuffed:
                    understaffed_teams_count += 1
                if rotation_members_count == unreachable_members_count:
                    unreachable_teams_count += 1

        self.ONCALL_TEAMS_UNDERSTAFFED.set(understaffed_teams_count)
        self.ONCALL_TEAMS_UNREACHABLE_BY_PHONE_TOTAL.set(unreachable_teams_count)

    def start_exporting(self):
        prometheus_client.REGISTRY.unregister(prometheus_client.GC_COLLECTOR)
        prometheus_client.REGISTRY.unregister(prometheus_client.PLATFORM_COLLECTOR)
        prometheus_client.REGISTRY.unregister(prometheus_client.PROCESS_COLLECTOR)
        start_http_server(port=self.config.port, addr=self.config.ip)
        logging.info(f"started http server on http://{self.config.ip}:{self.config.port}")

        while True:
            for call_scraper in self.scrapers:
                try:
                    call_scraper()
                    time.sleep(self.config.scrape_interval)
                except Exception as e:
                    logging.exception(e)


def terminate(signal, frame):
    print("Ctrl+C Terminating")
    sys.exit(0)


if __name__ == "__main__":
    config = ExporterConfig()
    signal.signal(signal.SIGINT, terminate)
    logging.basicConfig(format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%H:%M:%S',
                        level=config.log_level)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    Exporter(config).start_exporting()
